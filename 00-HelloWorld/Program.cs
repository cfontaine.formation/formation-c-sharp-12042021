﻿using System;

namespace _00_HelloWorld
{
    /// <summary>
    /// Une Classe Hello World
    /// </summary>
    class Program  // Commentaire fin de ligne
    {
        /// <summary>
        /// Point d'entrée du programme
        /// </summary>
        /// <param name="args">arguments de la ligne de commande</param>
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!!!");
            /*
             * Commentaire 
             * sur
             * plusieurs ligne
             */
            Console.ReadKey();
        }
    }
}
