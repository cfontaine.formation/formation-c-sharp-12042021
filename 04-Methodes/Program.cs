﻿using System;

namespace _04_Methodes
{
    class Program
    {
        static void Main(string[] args)
        {
            // Appel de methode
            int res = SommeEntier(1, 3);

            // Appel de methode (sans retour)
            AfficherValeur(2);
            Console.WriteLine(res);

            // Exercice maximum
            Console.WriteLine(Maximum(4, 1));
            Console.WriteLine(Maximum(1, 4));

            // Exercice parité 
            Console.WriteLine(Even(3));
            Console.WriteLine(Even(4));

            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int tv = 10;
            TestParamValeur(tv);
            Console.WriteLine(tv); // 10

            // Passage de paramètre par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            tv = 10;
            TestParamRef(ref tv);
            Console.WriteLine(tv); // 11

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée
            int o;
            TestParamOut(out o);
            Console.WriteLine(o); //110

            // Déclarer la variable de retour dans les arguments pendant l'appel de la méthode
            TestParamOut(out int o2);
            Console.WriteLine(o2); //110

            // On peut ignorer un paramètre out en le nommant _
            getTemp(out _, out int temp, out _);
            Console.WriteLine(temp);

            // Paramètre optionnel
            TestParamOptionnel(false);
            TestParamOptionnel(true, "Bonjour");
            TestParamOptionnel(true, "Bonjour", 134);
            // TestParamOptionnel(true, 134);

            // Paramètres nommés
            Maximum(v2: 34, v1: 3);
            TestParamOptionnel(test: true, i: 134);
            TestParamOptionnel(i: 134, test: true, str: "world");

            // Nombre de paramètres variable
            Console.WriteLine(Moyenne(2, 4, 6, 7, 5, 1));
            Console.WriteLine(Moyenne(2));
            Console.WriteLine(Moyenne(2, 5));
            int[] tab = { 23, 5, 7, 2, 5 };
            Console.WriteLine(Moyenne(2, tab));

            // Exercice Tableau
            Menu();

            // Surcharge
            Console.WriteLine(Multiplication(1, 2));
            Console.WriteLine(Multiplication(4.6, 2.5));
            Console.WriteLine(Multiplication(1, 2.4));
            Console.WriteLine(Multiplication(1, 2, 3));

            Console.WriteLine(Multiplication(4.6F, 2.5F));
            Console.WriteLine(Multiplication(1L, 2));
            Console.WriteLine(Multiplication('a', 2));
            //  Console.WriteLine(Multiplication(6M,2));

            // Affichage des paramètres passés au programme (main)
            // En lignes de commandes: 05-methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet-> onglet Déboguer-> options de démarrage -> Arguments de la ligne de commande 
            foreach (string a in args)
            {
                Console.WriteLine(a);
            }

            // Méthode récursive
            int f = Factorial(3);
            Console.WriteLine(f);

            Console.ReadKey();
        }
        static int SommeEntier(int a, int b)
        {
            return a + b;   // L'instruction return
                            // - Interrompt l'exécution de la méthode
                            // - Retourne la valeur (expression à droite)
        }

        static void AfficherValeur(int e) // void => pas de valeur retournée
        {
            Console.WriteLine(e);
            // avec void => return; ou return peut être omis 
        }

        #region Exercice Maximum
        // Ecrire une méthode Maximum qui prends en paramètre 2 nombres et elle retourne le maximum
        static int Maximum(int v1, int v2)
        {
            //if (v1 > v2)
            //{
            //    return v1;
            //}
            //else
            //{
            //    return v2;
            //}

            return v1 > v2 ? v1 : v2;
        }
        #endregion

        #region Exercice Parité
        // Écrire une méthode even qui prend un entier en paramètre
        // Elle retourne vrai, si il est paire
        static bool Even(int valeur)
        {
            //if (valeur % 2 == 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return valeur % 2 == 0;
        }
        #endregion

        #region Passage de paramètre

        // Passage par valeur
        static void TestParamValeur(int v)
        {
            Console.WriteLine(v);
            v++;    // La modification de la valeur du paramètre v n'a pas d'influence en dehors de la méthode
            Console.WriteLine(v);
        }

        // Passage par référence => ref
        static void TestParamRef(ref int v)
        {
            Console.WriteLine(v);
            v++;
            Console.WriteLine(v);
        }

        // Passage de paramètre en sortie => out
        static void TestParamOut(out int v)
        {
            //Console.WriteLine(v);
            v = 110;    // La méthode doit obligatoirement affecter une valeur aux paramètres out
            Console.WriteLine(v);
        }

        static void getTemp(out int min, out int current, out int max)
        {
            min = 10;
            current = 12;
            max = 20;
        }

        // On peut donner une valeur par défaut aux arguments passés par valeur
        static void TestParamOptionnel(bool test, string str = "hello", int i = 3)
        {
            Console.WriteLine($"test={test} str={str} i={i}");
        }

        // Nombre d'arguments variable => params
        static double Moyenne(int v1, params int[] valeurs)
        {
            double somme = v1;
            foreach (int elm in valeurs)
            {
                somme += elm;
            }
            return somme / (1 + valeurs.Length);
        }
        #endregion

        #region Surcharge_de_méthode
        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature

        static int Multiplication(int a, int b)
        {
            Console.WriteLine("2 Entiers");
            return a * b;
        }

        static double Multiplication(double a, double b)
        {
            Console.WriteLine("2 doubles");
            return a * b;
        }

        static double Multiplication(int a, double b)
        {
            Console.WriteLine("un entier un  doubles");
            return a * b;
        }

        static int Multiplication(int z, int e, int a)
        {
            Console.WriteLine("3 Entiers");
            return z * e * a;
        }
        #endregion

        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1)// condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }

        #region Exercice Tableau
        // Écrire un méthode qui affiche un tableau d’entier
        // Écrire une méthode qui permet de saisir :
        //  - La taille du tableau
        //  - Les éléments du tableau
        // Écrire une méthode qui calcule :
        // - le minimum d’un tableau d’entier
        // - le maximum
        // - la moyenne
        // Faire un menu qui permet de lancer ces méthodes
        static void AfficherTab(int[] tab)
        {
            Console.Write("[ ");
            for (int i = 0; i < tab.Length; i++)
            {
                Console.Write($"{tab[i]} ");
            }
            Console.WriteLine(" ]");
        }

        static int[] SaisirTab()
        {
            Console.WriteLine("Saisir la taille du tableau");
            int size = Convert.ToInt32(Console.ReadLine());

            int[] tab = new int[size];
            for (int i = 0; i < tab.Length; i++)
            {
                Console.Write($"tab[{i}]=");
                tab[i] = Convert.ToInt32(Console.ReadLine());
            }
            return tab;
        }
        static void CalculTab(int[] tab, out int min, out int max, out double moy)
        {
            min = int.MaxValue;
            max = int.MinValue;
            double somme = 0.0;
            foreach (int t in tab)
            {
                somme += t;
                if (t < min)
                {
                    min = t;
                }
                else if (t > max)
                {
                    max = t;
                }
            }
            moy = somme / tab.Length;
        }

        static void AfficherMenu()
        {
            Console.WriteLine("1 - Saisir le tableau");
            Console.WriteLine("2 - Afficher le tableau");
            Console.WriteLine("3 - Afficher le minimum, le maximum et la moyenne");
            Console.WriteLine("0 - Quitter");
        }

        static void Menu()
        {
            int choix;
            int[] tab = null;
            do
            {
                AfficherMenu();
                Console.WriteLine("Choix=");
                choix = Convert.ToInt32(Console.ReadLine());
                if (tab == null && (choix == 2 || choix == 3))
                {
                    Console.WriteLine("Le tableau n'est pas saisie");
                }
                else
                {
                    switch (choix)
                    {
                        case 1:
                            tab = SaisirTab();
                            break;
                        case 2:
                            AfficherTab(tab);
                            break;
                        case 3:
                            CalculTab(tab, out int minimum, out int maximum, out double moyenne);
                            Console.WriteLine($"Minimum={minimum} , Maximum={maximum}, Moyenne={moyenne}");
                            break;
                        case 0:
                            Console.WriteLine("Au revoir !");
                            break;
                        default:
                            Console.WriteLine($"Le choix {choix}n'existe pas\n");
                            AfficherMenu();
                            break;
                    }
                }
            }
            while (choix != 0);
            #endregion
        }
    }
}
