﻿using System;
using System.IO;

namespace _13_IO
{
    class Program
    {
        static void Main(string[] args)
        {
            #region DriveInfo
            // DriveInfo fournit des informations sur les lecteurs d'une machine
            DriveInfo[] drv = DriveInfo.GetDrives();
            foreach (DriveInfo d in drv)
            {
                Console.WriteLine("____________________");
                Console.WriteLine(d.Name);              // Nom du lecteur
                Console.WriteLine(d.TotalFreeSpace);    // Espace disponible sur le lecteur
                Console.WriteLine(d.TotalSize);         // Espace total du lecteur
                Console.WriteLine(d.DriveType);         // Système de fichiers du lecteur NTFS, FAT ..
                Console.WriteLine(d.DriveFormat);       // Type de lecteur Fixed Removable
            }
            #endregion

            #region Directory
            //  Teste si le dossier existe
            if (!Directory.Exists(@"c:\Formations\TestIO\CSharp"))
            {
                Directory.CreateDirectory(@"c:\Formations\TestIO\CSharp");  // Création du répertoire
            }

            string[] paths = Directory.GetDirectories(@"c:\Formations\TestIO\CSharp"); // Liste les fichiers du répertoire
            foreach (string p in paths)
            {
                Console.WriteLine(p);
            }
            paths = Directory.GetFiles(@"c:\Formations\TestIO\CSharp"); // Liste les répertoires contenu dans le chemin
            foreach (string p in paths)
            {
                Console.WriteLine(p);
            }
            #endregion

            #region File

            if (!File.Exists(@"c:\Formations\TestIO\CSharp\Test.txt"))  // Teste si le fichier n'existe pas
            {
                File.CreateText(@"c:\Formations\TestIO\CSharp\Test.txt");    // Crée le fichier a.txt s'il n'existe pas
            }
            if (File.Exists(@"c:\Formations\TestIO\CSharp\A.rtf"))
            {
                File.Move(@"c:\Formations\TestIO\CSharp\A.rtf", @"c:\Formations\TestIO\CSharp\B.rtf");   // On renomme le fichier A.rtf en fichier B.rtf
            }
            #endregion
            #region Stream
            EcrireFichierText(@"C:\Formations\TestIO\Csharp\test.txt");
            LireFichierText(@"c:\Formations\TestIO\Csharp\test.txt");

            EcrireFichierBin(@"c:\Formations\TestIO\Csharp\test.bin");
            LireFichierBin(@"c:\Formations\TestIO\Csharp\test.bin");

            // Exercice Parcourir
            Parcourir(@"C:\Formations\TestIO");

            // Exercice Copie
            Copier(@"C:\Formations\TestIO\Csharp\logo.png", @"C:\Formations\TestIO\Csharp\copid_logo.png");
            #endregion
            Console.ReadKey();

        }

        public static void EcrireFichierText(string path)
        {
            StreamWriter sw = null;     // StreamWriter Ecrire un fichier texte
            try                         // Sans utiliser Using  
            {
                sw = new StreamWriter(path, true);      // append à true "compléte" le fichier s'il existe déjà, à false le fichier est écrasé
                for (int i = 0; i < 10; i++)
                {
                    sw.WriteLine("Hello world");
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                sw.Close();
                sw.Dispose();
            }
        }


        public static void LireFichierText(string path)
        {
            // Using => Équivalent d'un try / finally + Close()
            using (StreamReader sr = new StreamReader(path))    // StreamReader Lire un fichier texte
            {
                while (!sr.EndOfStream)  // Propriété EndOfStream est vrai si le fichier atteint la fin du fichier
                {
                    Console.WriteLine(sr.ReadLine());
                }
            }
        }

        public static void EcrireFichierBin(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Append))   // FileStream =>  permet de Lire/Ecrire un fichier binaire
            {
                for (byte b = 0; b < 100; b++)
                {
                    fs.WriteByte(b);    // Ecriture d'un octet dans le fichier
                }
            }
        }

        public static void LireFichierBin(string path)
        {
            byte[] tab = new byte[10];
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                int nb = 1;
                while (nb != 0)
                {
                    nb = fs.Read(tab, 0, 10);   // Lecture de 10 octets au maximum dans le fichier, ils sont placés dans le tableau tab à partir de l'indice 0
                    foreach (var v in tab)      // Read => retourne le nombre d'octets lue dans le fichier                     {
                        Console.WriteLine(v);
                }
            }
        }

        static void Parcourir(string path)
        {
            if (Directory.Exists(path))
            {
                string[] fileNames = Directory.GetFiles(path);
                foreach (string f in fileNames)
                {
                    Console.WriteLine(f);
                }
                string[] directoryNames = Directory.GetDirectories(path);
                foreach (string d in directoryNames)
                {
                    Console.WriteLine($"Répertoire {d}");
                    Console.WriteLine("_____________");
                    Parcourir(d);
                }
            }
            else if (!File.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

        }
        static void Copier(string pathSource, string pathTarget)
        {
            FileStream fsi = null;
            FileStream fso = null;
            try
            {
                fsi = new FileStream(pathSource, FileMode.Open);
                fso = new FileStream(pathTarget, FileMode.CreateNew);
                int b = 1;
                while (b != -1)
                {
                    b = fsi.ReadByte();
                    if (b != -1)
                    {
                        fso.WriteByte((byte)b);
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                if (fsi != null)
                {
                    fsi.Close();
                    fsi.Dispose();
                }
                if (fso != null)
                {
                    fso.Close();
                    fso.Dispose();
                }
            }
        }
    }
}
