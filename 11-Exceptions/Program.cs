﻿using System;
using System.IO;

namespace _11_Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Saisir un index");
                int[] tab = new int[5];
                int index = Convert.ToInt32(Console.ReadLine()); // Peut générer une Exception FormatException, si on saisie autre chose qu'un nombre entier 
                tab[index] = 10;                                 // Peut générer une Exception IndexOutOfRangeException , si index est > 5
                Console.WriteLine("Saisir un age");
                int age = Convert.ToInt32(Console.ReadLine());   // Peut générer une Exception FormatException
                TraitementEmploye(age);
                // File.OpenRead("nexitepas");                   // Génère  une Exception FileNotFoundException
            }
            catch (FormatException e)   // Attrape les exceptions FormatException
            {
                Console.WriteLine("Mauvais format");
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine("En dehors des limite du tableau");
            }
            catch (IOException e)
            {
                Console.WriteLine("FileNotFoundException");
            }
            catch (Exception e)      // Attrape tous les autres exception
            {
                Console.WriteLine($"Les autres exceptions {e.Message}");    // Message => permet de récupérer le messsage de l'exception
                Console.WriteLine(e.StackTrace);
            }
            finally     // Le bloc finally est toujours éxécuté
            {
                Console.WriteLine("Toujours exécuter");
                // finally est utiliser pour libérer les ressources
            }
            Console.ReadKey();
        }

        static void TraitementEmploye(int age)
        {
            Console.WriteLine("Début du traitement Employé");
            try
            {
                TraitementAge(age);
            }    // Traitement partiel de l'exception AgeNegatifException 
            catch (AgeException e) when (e.Age > -10)     // when : le catch est éxécuté si la condition dans when est vrai
            {
                // traitement partial
                Console.WriteLine("Traitement Partiel age>-10");
                // On relance de l'exception
                throw;  // On relance l'exception pour que l'utilisateur de la méthode la traite a son niveau

            }
            catch (AgeException e)
            {
                Console.WriteLine("Traitement Partiel age<-10");
                // On relance une autre exception
                throw new Exception("Employe age négatif", e);  // e =>référence à l'exception qui a provoquer l'exception
            }
            Console.WriteLine("Fin du traitement Employé");
        }

        static void TraitementAge(int age)
        {
            Console.WriteLine("Début du traitement");
            if (age < 0)
            {
                // throw new Exception(string.Format($"L'age ({age})est négatif"));
                throw new AgeException(age, "L'age est négatif");   //  throw => Lancer un exception
                                                                    //  Elle va remonter la pile d'appel des méthodes jusqu'à ce qu'elle soit traité par un try/catch
            }                                                       //  si elle n'est pas traiter, aprés la méthode main => arrét du programme 
            Console.WriteLine("Fin du traitement");
        }
    }
}
