﻿using System;

namespace _11_Exceptions
{
    // On peut créer ses propres exceptions en héritant de la classe Exception
    // Par convention, toutes les sous-classes ont un nom se terminant par Exception
    class AgeException : Exception
    {
        public int Age { get; private set; }
        public AgeException(int age) : base()  // base => appel au constructeur de la classe Exception
        {
            Age = age;
        }

        public AgeException(int age, string message) : base(string.Format($"{message} : age={age}"))
        {
            Age = age;
        }

        public AgeException(int age, string message, Exception inner) : base(string.Format($"{message} : age={age}"), inner)
        {
            Age = age;
        }
    }
}
