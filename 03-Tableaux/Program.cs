﻿using System;

namespace _03_Tableaux
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Tableau à une dimension
            // int[] tab=null;
            // tab = new int[4];

            int[] tab = new int[4];

            // Valeur d'initialisation par défaut des éléments du tableau
            // - entier -> 0
            // - nombre à virgule flottante -> 0.0
            // - bool -> false
            // - char -> '\u0000'
            // - type référence -> null


            // Accèder à un élément du tableau
            Console.WriteLine(tab[0]);
            tab[0] = 12;
            Console.WriteLine(tab[0]);

            // Length=>Nombre d'élément du tableau
            Console.WriteLine(tab.Length);

            double[] tab2 = { 12.3, 4.5, -6.7 };
            Console.WriteLine(tab2[1]);

            // Parcourir complétement un tableau (avec un for)
            for (int i = 0; i < tab2.Length; i++)
            {
                Console.WriteLine($"tab[{i}]={tab2[i]}");
                //tab2[i] = 10.0;
            }

            // Parcourir complétement un tableau (foreach)
            foreach (var elm in tab2)
            {
                Console.WriteLine(elm);
                // elm = 2.3; // elm est accessible uniquement en lecture 
            }

            // tab2[10] = 9.0;   // si l'on essaye d'accèder à un élément en dehors du tableau => exception

            // La taille du tableau à la déclaration peut être une variable
            int s = 6;
            bool[] tabB = new bool[s];
            foreach (var elm in tabB)
            {
                Console.WriteLine(elm);
            }
            #endregion
            #region Exercice: Tableau
            // 1 - Trouver la valeur maximale et la moyenne d’un tableau de 5 entier: -7,4,8,0,-3
            // 2 - Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau
            Console.WriteLine("Saisir la taille du tableau");   // 2
            int size = Convert.ToInt32(Console.ReadLine());
            int[] tabV = new int[size];

            for (int i = 0; i < tabV.Length; i++)
            {
                Console.Write($"tab[{i}]=");
                tabV[i] = Convert.ToInt32(Console.ReadLine());
            }

            // int[] tabV = { -7, 4, 8, 0, -3 };       // 1
            int maximum = tabV[0];
            double somme = 0.0;
            foreach (var t in tabV)
            {
                if (t > maximum)
                {
                    maximum = t;
                }
                somme += t;
            }
            Console.WriteLine($"Maximum={maximum} Moyenne={somme / tabV.Length}");
            #endregion

            #region  Tableau Multidimmension
            int[,] tab2D = new int[3, 2];   // Déclaration d'un tableau à 2 dimensions

            tab2D[1, 0] = 4;    // Accès à un élémént d'un tableau à 2 dimensions

            Console.WriteLine(tab2D.Length);    // Nombre d'élément du tableau    => 6
            Console.WriteLine(tab2D.Rank);      // Nombre de dimension du tableau => 2
            Console.WriteLine($"Nombre de ligne={tab2D.GetLength(0)}");     // Nombre de ligne du tableau => 3
            Console.WriteLine($"Nombre de colonne={tab2D.GetLength(1)}");   // Nombre de colonne => 2

            // Parcourir complétement un tableau à 2 dimension => foreach
            for (int i = 0; i < tab2D.GetLength(0); i++)
            {
                for (int j = 0; j < tab2D.GetLength(1); j++)
                {
                    Console.WriteLine($"tab2D[{i},{j}]={tab2D[i, j]}");
                }
            }

            // Parcourir complétement un tableau à 2 dimension => foreach
            foreach (var elm in tab2D)
            {
                Console.WriteLine(elm);
                //elm = 34;
            }

            // Déclarer un tableau à 2 dimension en l'initialisant
            string[,] tabStr = { { "aze", "rty" }, { "uio", "pqs" }, { "dfg", "hjk" } };
            foreach (var elm in tabStr)
            {
                Console.WriteLine(elm);
            }

            // Tableau 3D
            // Déclaration d'un tableau à 3 dimensions
            int[,,] tab3D = new int[3, 2, 4];   // Accès à un élémént d'un tableau à 3 dimensions
            tab3D[1, 0, 2] = 12;
            Console.WriteLine(tab3D.Length);    // Nombre d'élément du tableau    => 24
            Console.WriteLine(tab3D.Rank);      // Nombre de dimension du tableau => 3
            #endregion

            #region Tableau de tableau
            // Déclaration d'un tableau de tableau
            char[][] tabChr = new char[4][];
            tabChr[0] = new char[3];
            tabChr[1] = new char[2];
            tabChr[2] = new char[4];
            tabChr[3] = new char[3];


            tabChr[2][2] = 'a';    // accès à un élément

            Console.WriteLine($"Nombre de ligne={tabChr.Length}");   // Nombre de Ligne => 3

            for (int i = 0; i < tabChr.Length; i++) // tabChr.getLength(0)    // Nombre de colonne pour chaque ligne
            {
                Console.WriteLine($"Nombre de colonne={tabChr[i].Length}");
            }

            for (int i = 0; i < tabChr.Length; i++)
            {
                for (int j = 0; j < tabChr[i].Length; j++)
                {
                    tabChr[i][j] = 'e';
                }
            }

            //  Parcourir complétement un tableau de tableau
            foreach (var row in tabChr)     // var => char[]
            {
                foreach (var elm in row)    // var => char
                {
                    Console.Write(elm);
                }
                Console.Write('\n');
            }

            int[][] tabMulti2 = new int[][] { new int[] { 10, 3, 4, 1, 5 }, new int[] { 12, -9 }, new int[] { 1, -1, 12 } };
            foreach (var row in tabMulti2) //int[]
            {
                foreach (var elm in row) //int
                {
                    Console.Write(elm);
                }
                Console.Write('\n');
            }
            #endregion
            Console.ReadKey();
        }
    }
}
