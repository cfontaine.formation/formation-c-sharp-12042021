﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Polymorphisme
{
    class Chenil
    {
        public IPeutMarcher[] resident = new IPeutMarcher[10];

        public void Promenade()
        {
            foreach(var r in resident)
            {
                r.Marcher();
            }
        }
    }
}
