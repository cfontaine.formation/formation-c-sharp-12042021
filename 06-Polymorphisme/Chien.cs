﻿using System;
using System.Collections.Generic;

namespace _06_Polymorphisme
{
    class Chien : Animal, IPeutMarcher  // La classe Chien hérite de la classe Animal et implémente l'interface IPeutMarcher
    {
        public string Nom { get; set; }
        public Chien(string nom, int age, double poid) : base(age, poid) => Nom = nom;
        //{
        //    
        //}

        public sealed override void EmmettreSon()        // Redéfinition obligatoire de la méthode abstraite EmmettreUnSon de la classe Animal
        {                                         // sealed sur une méthode interdit la redéfiniton de la méthode dans les sous-classes
            Console.WriteLine($"{Nom} aboie");
        }
        public void Marcher()   // Impléméntation de la méthode Marcher de l'interface IPeutMarcher
        {
            Console.WriteLine($"{Nom} marche");
        }

        // Impléméntation de la méthode Courrir de l'interface IPeutMarcher
        public void Courir() => Console.WriteLine($"{Nom} court");
        //{
        //    Console.WriteLine($"{Nom} court");
        //}

        public override string ToString() => string.Format($"Chien[{base.ToString()}, Nom ={Nom}]");
        //{
        //    return string.Format($"Chien[{base.ToString()}, Nom ={Nom}]");
        //}

        // Redéfinition des Méthodes d'object
        public override bool Equals(object obj)
        {
            return obj is Chien chien &&
                   base.Equals(obj) &&
                   Age == chien.Age &&
                   Poid == chien.Poid &&
                   Nom == chien.Nom;
        }

        public override int GetHashCode()
        {
            int hashCode = 1240350572;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + Age.GetHashCode();
            hashCode = hashCode * -1521134295 + Poid.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Nom);
            return hashCode;
        }

        public void Deplacer()
        {
            Console.WriteLine($"{Nom} se déplace");
        }
    }
}
