﻿namespace _06_Polymorphisme
{
    abstract class Animal   // Animal est une classe abstraite, elle ne peut pas être instantiée elle même
    {                       // mais uniquement par l'intermédiaire de ses classses filles
        public int Age { set; get; }
        public double Poid { set; get; }


        public Animal(int age, double poid)
        {
            Age = age;
            Poid = poid;
        }

        //public virtual void EmmettreSon()
        //{
        //    Console.WriteLine("L'animal emet un son");
        //}
        public abstract void EmmettreSon(); // Méthode abstraite qui doit obligatoirement redéfinit par les classes filles

        // Redéfinition des Méthodes d'object
        // ToString => retourne une chaîne de caractères qui représente l'objet
        public override string ToString()
        {
            return string.Format($"Animal [ Age= {Age} Poid={Poid}]");
        }

        // Equals => permet de tester si l'objet est égal à l'objet passer en paramètre
        // Si on redéfinie Equals, il faut aussi redéfinir GetHashCode
        public override bool Equals(object obj)
        {
            return obj is Animal animal &&
                   Age == animal.Age &&
                   Poid == animal.Poid;
        }

        public override int GetHashCode()
        {
            int hashCode = -368450247;
            hashCode = hashCode * -1521134295 + Age.GetHashCode();
            hashCode = hashCode * -1521134295 + Poid.GetHashCode();
            return hashCode;
        }
    }
}
