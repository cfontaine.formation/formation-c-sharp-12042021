﻿using System;
using System.Reflection;

namespace _06_Polymorphisme
{
    class Program
    {
        static void Main(string[] args)
        {
            //Animal a1 = new Animal(5, 3000);       // Impossible la classe est abstraite
            //a1.EmmettreSon();

            Chien c1 = new Chien("rolo", 4, 5000);

            c1.EmmettreSon();

            Animal a2 = new Chien("Laika", 2, 3000);    //  On peut créer une instance de Chien (classe fille) qui aura une référence, une référence Animal (classe mère)
                                                        // L'objet Chien sera vu comme un Animal, on ne pourra pas accèder aux propriétés et aux méthodes propre au chien (Nom,...)
            a2.EmmettreSon();                           // Comme la méthode est virtual dans Animal et est rédéfinie dans Chien, c'est la méthode de Chien qui sera appelée
            //if (a1 is Chien)           // test si a2 est de "type" Chien
            //{
            // Pour passer d'une super-classe à une sous-classe, il faut le faire explicitement avec un cast ou avec l'opérateur as
            // Chien ch2 =(Chien) a2;   
            //    Chien c2 = a1 as Chien;       // as equivalant à un cast pour un objet
            //    Console.WriteLine(c2.Nom);    // avec la référence ch2 (de type Chien), on a bien accès à toutes les propriétées de la classe Chien
            //}

            if (a2 is Chien c3)     // Avec l'opérateur is, on peut tester si a2 est de type Chien et faire la conversion en même temps
            {
                Console.WriteLine(c3.Nom);
            }
            Zoo zoo = new Zoo();
            zoo.pensionaire[0] = new Chien("rolo", 4, 5000);
            zoo.pensionaire[1] = new Chat(3, 7, 3000);
            zoo.pensionaire[2] = new Chien("Idefix", 8, 2000);
            zoo.pensionaire[3] = new Chat(5, 2, 2000);
            zoo.pensionaire[4] = new Chien("Laika", 2, 3000);

            zoo.Ecouter();

            // object
            // ToString
            Chien c4 = new Chien("Snoppy", 8, 2400);
            Console.WriteLine(c4);
            // Equals GetHashCode
            Chien c5 = new Chien("Snoppy", 8, 2400);
            Console.WriteLine(c4 == c5); // si l'opérateur == n'est pas redéfinit, on compare l'égalité des références comme se sont 2 objets différents => false
            Console.WriteLine(c4.Equals(c5));
            // Type
            //  Type type = c4.GetType();
            Type type = typeof(Chien);
            Console.WriteLine(type.Name);
            PropertyInfo[] props = type.GetProperties(); // Reflexion
            foreach (var p in props)
            {
                Console.WriteLine(p.Name);
                Console.WriteLine(p.GetType().Name);
                Console.WriteLine(p.GetValue(c4).ToString());
            }

            // Interface
            IPeutMarcher ipm1 = new Chien("rolo", 4, 5000);  // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            ipm1.Courir();
            ipm1.Marcher();
            ipm1.Deplacer();

            IPeutMarcher ipm2 = new Canard(2, 3000);
            ipm2.Deplacer();

            IPeutVoler ipv1 = new Canard(2, 3000);
            ipv1.Deplacer();

            Console.ReadKey();
        }
    }
}
