﻿using System;

namespace _06_Polymorphisme
{
    // Un interface ne définit que des méthodes qui devront être obligatoirement implémenté dans la classe qui va impléménter l'interface
    // Un interface commence toujours par convention par I
    interface IPeutMarcher
    {
        void Marcher();
        void Courir();
        void Deplacer();
        //void MethodDefaut()
        //{
        //    Console.WriteLine("Méthode par défaut");
        //}
    }
}
