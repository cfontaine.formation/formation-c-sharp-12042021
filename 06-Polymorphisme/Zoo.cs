﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Polymorphisme
{
    class Zoo
    {
        public Animal[] pensionaire = new Animal[10];
        
        public void Ecouter()
        {
            foreach(Animal an in pensionaire)
            {
                if (an != null)
                {
                    an.EmmettreSon();
                }
            }
        }
    }
}
