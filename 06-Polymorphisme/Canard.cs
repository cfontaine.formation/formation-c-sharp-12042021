﻿using System;

namespace _06_Polymorphisme
{
    class Canard : Animal, IPeutMarcher, IPeutVoler   // On peut implémenter plusieurs d'interfaces
    {
        public Canard(int age, double poid) : base(age, poid)
        {
        }

        public override void EmmettreSon()
        {
            Console.WriteLine("Coin coin");
        }

        public void Marcher()
        {
            Console.WriteLine($"Le canard  marche");
        }

        public void Courir()
        {
            Console.WriteLine($"Le canard court");
        }

        public void Atterir()
        {
            Console.WriteLine($"Le canard  attérit");
        }

        public void Decoller()
        {
            Console.WriteLine($"Le canard décolle");
        }

        void IPeutMarcher.Deplacer()
        {
            Console.WriteLine($"Le canard se déplace en marchant");
        }

        void IPeutVoler.Deplacer()
        {
            Console.WriteLine($"Le canard se déplace en volant");
        }

    }
}
