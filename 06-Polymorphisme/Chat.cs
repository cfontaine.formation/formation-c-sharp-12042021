﻿using System;

namespace _06_Polymorphisme
{
    class Chat : Animal, IPeutMarcher
    {
        public int NbVie { get; set; } = 9;
        public Chat(int nbVie, int age, double poid) : base(age, poid)
        {
            NbVie = nbVie;
        }

        public override void EmmettreSon()
        {
            Console.WriteLine("Le chat Miaule");
        }

        public void Marcher()
        {
            Console.WriteLine($"Le chat  marche");
        }

        public void Courir()
        {
            Console.WriteLine($"Le chat  court");
        }

        public void Deplacer()
        {
            Console.WriteLine($"Le chat se déplace");
        }
    }
}
