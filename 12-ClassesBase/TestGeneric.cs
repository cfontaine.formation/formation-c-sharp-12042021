﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_ClassesBase
{
    class TestGeneric<T> //where T : struct 
    {
        public T a { get; set; }
        public T b { get; set; }

        public TestGeneric(T a, T b)
        {
            this.a = a;
            this.b = b;
        }

        public void Afficher()
        {
            Console.WriteLine($"{a} {b}");
        }
    }
}
