﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _12_ClassesBase
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Chaine de caractère
            string str = "Hello";
            string str2 = new string('a', 10);
            Console.WriteLine(str);
            Console.WriteLine(str2);

            // Length -> Nombre de caractère de la chaine de caractère
            Console.WriteLine(str2.Length);

            // On peut accèder à un caractère de la chaine comme à un élément d'un tableau (index commence à 0)
            Console.WriteLine(str[1]); // e

            // Concaténation 
            // avec l'opérateur + ou avec la méthode de classe Concat
            string str3 = string.Concat(str, " World");
            Console.WriteLine(str3);

            // La méthode Join concatènent les chaines, en les séparants par un caractère (ou une chaine) de séparation
            string csv = string.Join(";", "azerty", "yuio", "sdfgh");
            Console.WriteLine(csv);

            // Découpe la chaine en un tableau de sous-chaine suivant un ou plusieurs séparateur, par défaut le caractère espace
            string calcul = "12 + 30";
            string[] elm = calcul.Split(' ');
            foreach (string s1 in elm)
            {
                Console.WriteLine(s1);
            }

            // Substring permet d'extraire une sous-chaine d'une chaine
            // de l'indice passé en paramètre juqu'à la fin de la chaine
            Console.WriteLine(str3.Substring(6));
            // ou pour le nombre de caractères passé en paramètre
            Console.WriteLine(str3.Substring(4, 3));

            // Insère la chaine à partir de l'indice passé en paramètre
            Console.WriteLine(str3.Insert(5, "------"));

            // Insère la chaine à partir de l'indice passé en paramètre
            Console.WriteLine(str3.Remove(5, 1));

            // StartsWith retourne true si la chaine commence par la chaine passé en paramètre
            Console.WriteLine(str3.StartsWith("Hell"));
            Console.WriteLine(str3.StartsWith("aaaa"));

            // IndexOf retourne la première occurence du caractère ou de la chaine passée en paramètre
            Console.WriteLine(str3.IndexOf('o'));       //4
            Console.WriteLine(str3.IndexOf('o', 5));     //7 idem mais à partir de l'indice passé en paramètre
            Console.WriteLine(str3.IndexOf('o', 8));    // retourne -1, si le caractère n'est pas trouvé 

            // Remplace toutes les chaines (ou caratère) oldValue par newValue 
            Console.WriteLine(str3.Replace('o', 'a'));

            // Retourne True si la sous-chaine passée en paramètre est contenu dans la chaine
            Console.WriteLine(str3.Contains("lo"));
            Console.WriteLine(str3.Contains("aaa"));

            // Aligne les caractères à droite en les complétant par un caractère à gauche pour une longueur spécifiée
            Console.WriteLine(str3.PadLeft(50, '_'));

            // Aligne les caractères à gauche en les complétant par un caractère à droite pour une longueur spécifiée
            Console.WriteLine(str3.PadRight(50, '_'));

            // ToUpper convertie tous les caractères en majuscule
            Console.WriteLine(str3.ToUpper());

            // ToLower convertie tous les caractères en minuscule
            Console.WriteLine(str3.ToLower());

            // Trim supprime les caractères de blanc du début et de la fin de la chaine
            string str4 = "            \n \t   azerty   uiipooop \n \t  ";
            Console.WriteLine(str4.Trim());
            Console.WriteLine(str4.TrimStart());    // idem uniquement en début de chaine
            Console.WriteLine(str4.TrimEnd());      // idem uniquement en fin de chaine

            // On peut chainer l'appel des différentes méthodes
            Console.WriteLine(str3.ToLower().TrimEnd().Substring(5));

            // On peut tester l'égalité de 2 chaines de caractères avec Equals et l'opérateur ==
            Console.WriteLine("Hello".Equals(str));
            Console.WriteLine("Hello" == str);

            // Comparaison 0-> égale, 1-> se trouve après, -1 -> se trouve avant dans l'ordre alphabétique
            // Il existe 2 méthodes: une d'instance et de classe
            Console.WriteLine(str.CompareTo("Bonjour"));
            Console.WriteLine(string.Compare("Bonjour", str));

            // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation et insertion, remplacement,supression de sous-chaine)
            // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un objet String => pas de création d'objet intermédiare
            StringBuilder sb = new StringBuilder("test");
            sb.Append(42);
            sb.Append("Hello");
            string str5 = sb.ToString();    // Convertion d'un StringBuilder en une chaine de caractères 
            Console.WriteLine(str5);

            // Exercice Inversion de chaine
            Console.WriteLine(Inverser("Bonjour"));

            // Exercice Palindrome
            Console.WriteLine(Palindrome("Bonjour"));
            Console.WriteLine(Palindrome("Radar   "));

            // Exercice Acronyme
            Console.WriteLine(Acronyme("Organisation du traité de l’Atlantique Nord"));
            Console.WriteLine(Acronyme("Développement d’application web en aglomération nantaise"));

            #endregion

            #region Date
            DateTime d = DateTime.Now;  // DateTime.Now => récupérer l'heure et la date courante
            Console.WriteLine(d);
            Console.WriteLine(d.Year);
            Console.WriteLine(d.Hour);
            DateTime dUtc = DateTime.UtcNow;
            Console.WriteLine(dUtc);

            DateTime noel2015 = new DateTime(2015, 12, 25);
            Console.WriteLine(noel2015);
            DateTime debut = new DateTime(2021, 04, 12, 9, 30, 0);
            Console.WriteLine(d - debut);

            TimeSpan dixJours = new TimeSpan(10, 0, 0, 0);  // TimeSpan => représente une durée
            Console.WriteLine(d.Add(dixJours));
            Console.WriteLine(d.AddHours(5));
            Console.WriteLine(d.ToLongDateString());
            Console.WriteLine(d.ToShortDateString());
            Console.WriteLine(d.ToLongTimeString());
            Console.WriteLine(d.ToShortTimeString());
            Console.WriteLine(d.ToString("yy/MM/dd"));
            Console.WriteLine(DateTime.Parse("2003/07/12"));
            #endregion

            #region Collection
            // Collection faiblement typé => elle peut contenir tous types d'objets
            ArrayList lst = new ArrayList();
            lst.Add("azerty");
            lst.Add(123);
            double da = 123.5;
            lst.Add(da);
            lst.Add(true);
            Console.WriteLine(lst[0]);
            if (lst[3] is string strA)
            {
                Console.WriteLine(strA);
            }
            Console.WriteLine(lst.Count);     // Nombre d'élément de la collection


            // Collection fortement typée => type générique
            List<int> lstInt = new List<int>();
            lstInt.Add(3);
            lstInt.Add(45);
            lstInt.Add(-7);
            lstInt.Add(7);
            //lstInt.Add("azettry");        // On ne peut plus qu'ajouter des entiers => sinon erreur de complilation
            int val = lstInt[0];            // Accès à un élément de la liste
            lstInt[0] = 678;

            foreach (var v in lstInt)    // Parcourir la collection complétement
            {
                Console.WriteLine(v);
            }

            Console.WriteLine(lstInt.Min());    // Valeur minimum stocké dans la liste 
            lstInt.Reverse();                   // Inverser l'ordre de la liste 
            foreach (var v in lstInt)
            {
                Console.WriteLine(v);
            }
            lstInt.Insert(1, 4);

            // Parcourrir un collection avec un Enumérateur
            var it = lstInt.GetEnumerator();
            while (it.MoveNext())
            {
                Console.WriteLine(it.Current);
            }

            // Iterator
            foreach (var n in Numbers())
            {
                Console.WriteLine(n);
            }

            /// Dictionary => association clé/valeur
            Dictionary<int, string> m = new Dictionary<int, string>();
            m.Add(12, "AZERYY");    // Add => ajout d'un valeur associé à une clé
            // m.Add(12, "Other");   // On ne peut pas ajouter si la clé éxiste déjà => exception
            m.Add(45, "Bonjour");
            m.Add(65, "Hello");
            m.Add(347, "World");
            m.Add(5, "Asupprimer");
            m.Add(657, "John Doe");

            Console.WriteLine(m[65]);     // accès à un élément m[clé] => valeur
            m[65] = "Test";
            m.Remove(5);
            Console.WriteLine(m.Count);

            // Parcourir un dictionnary
            foreach (var kvp in m)  // KeyValuePair<int,string>
            {
                Console.WriteLine($"{kvp.Key} {kvp.Value}");
            }

            // Queue => pile FIFO
            Queue<int> q = new Queue<int>();
            q.Enqueue(1);
            q.Enqueue(2);
            q.Enqueue(3);
            q.Enqueue(4);
            Console.WriteLine(q.Peek());
            Console.WriteLine(q.Count);
            Console.WriteLine(q.Dequeue());
            Console.WriteLine(q.Count);
            Console.WriteLine(q.Dequeue());
            Console.WriteLine(q.Dequeue());
            Console.WriteLine(q.Dequeue());

            // Stack => pile LIFO
            Stack<int> s = new Stack<int>();
            s.Push(1);
            s.Push(2);
            s.Push(3);
            s.Push(4);
            Console.WriteLine(s.Peek());
            Console.WriteLine(s.Count);
            Console.WriteLine(s.Pop());
            Console.WriteLine(s.Count);
            Console.WriteLine(s.Pop());
            Console.WriteLine(s.Pop());
            Console.WriteLine(s.Pop());
            #endregion

            #region Generique
            // Classe générique
            TestGeneric<string> gen1 = new TestGeneric<string>("azerty", "uiop");
            gen1.Afficher();
            TestGeneric<double> gen2 = new TestGeneric<double>(12.6, 6.7);
            gen2.Afficher();

            // Méthode générique
            TestMethodeGenerique.MethodeGenerique<string>("azeerty");
            TestMethodeGenerique.MethodeGenerique("azeerty"); // Le type est déduit du type du paramètre
            TestMethodeGenerique.MethodeGenerique(true);
            #endregion
            Console.ReadKey();
        }

        static IEnumerable<int> Numbers()
        {
            for (int i = 10; i < 40; i++)
            {
                yield return i;
            }
        }
        // Exercice Inversion de chaine
        // Écrire la fonction Inversee qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
        // bonjour →  ruojnob
        static string Inverser(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = str.Length - 1; i >= 0; i--)
            {
                sb.Append(str[i]);
            }
            return sb.ToString();
        }

        // Exercice  Palindrome
        // Écrire une méthode __Palindrome__ qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
        //    SOS →  true
        //    Bonjour →  false
        //    radar →  true
        static bool Palindrome(string str)
        {
            string tmp = str.ToLower().Trim();
            return tmp == Inverser(tmp);
        }

        //Exercice Faire une méthode de classe qui prend en paramètre une phrase et qui retourne un acronyme
        //Les mots de la phrase sont pris en compte s'ils contiennent plus de 2 lettres
        //Ex:   Comité international olympique → CIO
        //      Organisation du traité de l’Atlantique Nord → OTAN
        static string Acronyme(string str)
        {
            string acr = "";
            string[] tabStr = str.Trim().ToUpper().Replace('’', ' ').Split();
            foreach (string s in tabStr)
            {
                if (s.Length > 2)
                {
                    acr += s[0];
                }
            }
            return acr;
        }


    }
}
