﻿using System;

namespace _12_ClassesBase
{
    class TestMethodeGenerique
    {
        public static void MethodeGenerique<T>(T a)
        {
            Console.WriteLine(a);
        }
    }
}
