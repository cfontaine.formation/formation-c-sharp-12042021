﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_delegate
{
    class CompteBancaire
    {
        public event EventHandler OnDecouvert;
        public double Solde { get; protected set; } = 50.0;
        public string Iban { get; }
        public string Titulaire { get; set; }

        private static int compteurCompte;

        public CompteBancaire()
        {
            compteurCompte++;
            Iban = "fr-5962-0000-" + compteurCompte;
        }

        public CompteBancaire(string titulaire) : this()
        {
            Titulaire = titulaire;
        }

        public CompteBancaire(double solde, string titulaire) : this(titulaire)
        {
            Solde = solde;
        }

        public void Crediter(double val)
        {
            if (val > 0.0)
            {
                Solde += val;
            }
        }

        public void Debiter(double val)
        {
            if (val > 0.0)
            {
                Solde -= val;
            }
            if (Solde < 0 && OnDecouvert != null)
            {
                OnDecouvert(this, EventArgs.Empty);
            }
        }

        public bool EstPositif()
        {
            return Solde >= 0;
        }

        public void Afficher()
        {
            Console.WriteLine("_________________________________");
            Console.WriteLine($"Solde= {Solde}");
            Console.WriteLine($"Iban= {Iban}");
            Console.WriteLine ($"Titulaire={Titulaire} ");
            Console.WriteLine("_________________________________");
        }
    }
}
