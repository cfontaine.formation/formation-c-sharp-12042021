﻿using System;
using System.Text.RegularExpressions;

namespace _10_delegate
{
    class Program
    {
        // Déléguation => Définition de prototypes de fonctions
        public delegate int Operation(int a, int b);
        public delegate bool Comparaison(int a, int b);

        // C# 1
        // Méthodes correspondants au prototype Operation
        // Retourne un entier et a pour paramètre 2 entiers
        public static int Ajouter(int n1, int n2)
        {
            return n1 + n2;
        }

        public static int Multiplier(int n1, int n2)
        {
            return n1 * n2;
        }

        static void Main(string[] args)
        {
            // Appel d'un délégué C# 1
            Operation op1 = new Operation(Ajouter);
            AfficherCalcul(1, 2, op1);
            AfficherCalcul(3, 2, new Operation(Multiplier));

            // Appel d'un délégué C# 2.0 => plus besoin de méthode correspondant au prototype
            Operation op2 = delegate (int n1, int n2) { return n1 + n2; };
            AfficherCalcul(1, 2, op2);
            AfficherCalcul(4, 5, delegate (int a, int b) { return a * b; });

            // Appel d'un délégué C# 3.0 => utilisation des lambdas
            // Lambda =>méthode sans nom, les paramètres et le corps de la méthode sont séparés par l'opérateur =>

            // Syntaxe
            // () => expression     S'il n'y a pas de paramètre
            // 1 paramètre => expression
            // (paramètres) => expression
            // (paramètres) => { instructions }
            AfficherCalcul(3, 4, (n1, n2) => n1 + n2);
            AfficherCalcul(3, 4, (n1, n2) => n1 * n2);

            // Exercice delegate
            int[] tab = { 1, -3, 7, 2, -4, 2 };
            SortTab(tab, (n1, n2) => n1 > n2);
            AfficherTab(tab);

            #region Evenement
            // La méthode débiter de Compte bancaire va générer un événement OnDecouvert quand le solde est <0
            CompteBancaire cb = new CompteBancaire(100.0, "John Doe");
            // S'abonner à l'événement
            cb.OnDecouvert += EnvoieMail;
            cb.OnDecouvert += EnvoieSMS;
            cb.Debiter(50.0);
            cb.Afficher();
            cb.Crediter(200.0);
            cb.Afficher();
            cb.Debiter(400.0);  // Lorsque le solde du compte est <0 l'événement OnDecouvert est lancé et les méthodes qui sont abonnées à cet événement sont executé (EnvoieMail,EnvoieSMS)
            #endregion

            #region Expression réguliaire
            string saisie = Console.ReadLine();
            // @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"
            Regex rag = new Regex("[a-w]+");
            if (rag.IsMatch(saisie))
            {
                Console.WriteLine("égalité");
            }
            #endregion
            Console.ReadKey();
        }

        // On utilise les délégués pour passer une méthode en paramètre à une autre méthode
        public static void AfficherCalcul(int a, int b, Operation op)
        {
            Console.WriteLine(op(a, b));
        }

        // Excercice delegate
        //- Ecrire le delegate Comparaison pour avoir un tri croissant ou décroissant
        //- Ecrire un lamba pour tri décroissant
        public static void SortTab(int[] tab, Comparaison cmp)
        {
            bool notDone = true;
            int end = tab.Length - 1;
            while (notDone)
            {
                notDone = false;
                for (int i = 0; i < end; i++)
                {
                    if (cmp(tab[i], tab[i + 1]))
                    {
                        int tmp = tab[i];
                        tab[i] = tab[i + 1];
                        tab[i + 1] = tmp;
                        notDone = true;
                    }
                }
                end--;
            }
        }

        static void AfficherTab(int[] tab)
        {
            Console.Write("[ ");
            foreach (int v in tab)
            {
                Console.Write($"{v} ");
            }
            Console.WriteLine(" ]");
        }

        static void EnvoieMail(object sender, EventArgs e)
        {
            Console.WriteLine("Mail : il n'y a plus d'argent sur votre compte");
            if (sender is CompteBancaire cb)
            {
                cb.Afficher();
            }
        }
        static void EnvoieSMS(object sender, EventArgs e)
        {
            Console.WriteLine("SMS: il n'y a plus d'argent sur votre compte");
            if (sender is CompteBancaire cb)
            {
                cb.Afficher();
            }
        }
    }
}
