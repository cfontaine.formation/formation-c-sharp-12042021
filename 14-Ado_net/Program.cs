﻿using _15_Bibliotheque;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace _14_Ado_net
{
    class Program
    {
        static void Main(string[] args)
        {
            // TestBdd();
            TestDao();
            Console.ReadKey();
        }

        static void TestBdd()
        {
            string chaineConnection = "Server=localhost;Port=3306;Database=formation;Uid=root;Pwd=dawan;";
            MySqlConnection cnx = new MySqlConnection(chaineConnection);
            cnx.Open();
            string req1 = "INSERT INTO contacts(prenom,nom,jour_naissance,email) VALUES (@prenom,@nom,@dateNaissance,@email);";
            MySqlCommand cmd = new MySqlCommand(req1, cnx);
            cmd.Parameters.AddWithValue("@prenom", "Jo");
            cmd.Parameters.AddWithValue("@nom", "Dalto,");
            cmd.Parameters.AddWithValue("@dateNaissance", DateTime.Now);
            cmd.Parameters.AddWithValue("@email", "jo@dawan.com");
            cmd.ExecuteNonQuery();
            Console.WriteLine(cmd.LastInsertedId);
            cnx.Close();
        }

        static void TestDao()
        {
            //Récupération de la chaine de connection dans le fichier App.config du projet élément<connectionStrings>
            ContactDao.ChaineConnection = ConfigurationManager.ConnectionStrings["chCnxMysql"].ConnectionString;
            ContactDao dao = new ContactDao();
            Contact c = SaisirContact();
            Console.WriteLine("id={0}", c.Id); // id=0 => l'objet n'a pas été peristé dans la base de donnée
            dao.SaveOrUpdate(c, true); // persister l'objet dans la bdd
            Console.WriteLine("id={0}", c.Id); // id a été généré par la bdd
            Console.WriteLine("{0}", c);
            long id = c.Id;
            // Affichage de tous les objets contact
            List<Contact> lst = dao.FindAll(true);
            foreach (Contact co in lst)
            {
                Console.WriteLine(co);
            }

            // Lire un objet à partir de son id
            Console.WriteLine("\nLire {0}", id);
            Contact cr = dao.FindById(id, true);
            Console.WriteLine(cr);

            // Modification 
            Console.WriteLine("\nModification {0}", cr.Id);
            cr.Prenom = "Marcel";
            cr.DateNaissance = new DateTime(1987, 8, 11);
            dao.SaveOrUpdate(cr, true);
            cr = dao.FindById(cr.Id, true);
            Console.WriteLine(cr);

            // Effacer
            Console.WriteLine("\neffacer {0}", cr.Id);
            dao.Delete(cr, true);
            lst = dao.FindAll();
            foreach (Contact co in lst)
            {
                Console.WriteLine(co);
            }

        }

        private static Contact SaisirContact()
        {
            Console.Write("Entrer votre prénom: ");
            string prenom = Console.ReadLine();
            Console.Write("Entrer votre nom: ");
            string nom = Console.ReadLine();
            Console.Write("Entrer votre date de naissance (YYYY/MM/DD): ");
            DateTime jdn = DateTime.Parse(Console.ReadLine());
            Console.Write("Entrer votre email: ");
            string email = Console.ReadLine();
            return new Contact(prenom, nom, jdn, email);
        }

    }
}
