﻿namespace _05_poo
{

    // Exercice Héritage
    // classe CompteEpargne qui hérite CompteBancaire
    // - une propriété taux
    // - Une méthode void calculInterets qui calcule le nouveau solde : solde=solde* (1+taux/100);
    // - Ajouter deux constructeurs:
    //    - qui a pour paramètre le taux et le titulaire
    //    - qui a pour paramètre le taux

    class CompteEpargne : CompteBancaire
    {
        public double Taux { get; set; } = 0.75;
        public CompteEpargne(double taux) // :base() est implicite
        {
            Taux = taux;
        }

        public CompteEpargne(Personne titulaire, double taux) : base(titulaire)
        {
            Taux = taux;
        }

        public void CalculInterets()
        {
            Solde *= (1 + Taux / 100.0);
        }
    }
}
