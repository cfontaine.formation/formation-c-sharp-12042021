﻿using System;

namespace _05_poo
{
    class Conteneur
    {
        private static string varClasse = "Variable de classe";

        private string varInstance = "Variable d'instance";

        public void TestImbrique()
        {
            Element e = new Element();
            e.TestClasse();
            e.TestInstance(this);
            Element.TestClasse2();
        }

        // Classe imbriquée
        private class Element   // par défaut private
        {

            public void TestClasse()
            {
                Console.WriteLine(varClasse);   // On a accès aux variables de la classe de la classe conteneur
            }
            public static void TestClasse2()
            {
                Console.WriteLine(varClasse);
            }

            public void TestInstance(Conteneur c)
            {
                Console.WriteLine(c.varInstance);
            }

        }

    }
}
