﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    class VoiturePrioritaire : Voiture
    {
        public VoiturePrioritaire(bool gyro) : base()
        {
            Gyro = gyro;
        }

        public VoiturePrioritaire(string marque, string couleur, int vitesse, bool gyro) : base(marque, couleur, vitesse)
        {
            Gyro = gyro;
        }

        public VoiturePrioritaire(string marque, string couleur, string plaqueIma) : base(marque, couleur, plaqueIma, 0, 0)
        {
        }

        public bool Gyro { get; private set; }


    

        public void AllumerGyro()
        {
            Gyro = true;
            Marque = "honda";
            Arreter();
        }

        public void EteindreGyro()
        {
            Gyro = false;
        }

        public override void Afficher()
        {
            base.Afficher();
            Console.WriteLine($"Gyro: {Gyro}");
        }

    }
}
