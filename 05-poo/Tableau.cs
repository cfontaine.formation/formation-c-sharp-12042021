﻿using System;

namespace _05_poo
{
    class Tableau
    {
        private int[] _elements;

        public int Length
        {
            get => _elements.Length;
        }
        public Tableau(int size)
        {
            _elements = new int[size];
        }

        // Indexeur
        public int this[int index]
        {
            get
            {
                if (index < _elements.Length)
                {
                    return _elements[index];
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            set
            {
                if (index < _elements.Length)
                {
                    _elements[index] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }

            }
        }
        // On peut avoir plusieurs indexeurs dans une classe, il faut que le type et le nombre de paramètre soit diférent (surcharge)
        //public int this[int ligne ,int colonne ]
        //{
        //    get
        //    {
        //        return 0;
        //    }
        //    set
        //    {


        //    }
        //}

        public void Add(int valeur)
        {
            resize(_elements.Length + 1);
            _elements[_elements.Length - 1] = valeur;
        }

        private void resize(int nSize)
        {
            int[] tmp = new int[nSize];
            for (int i = 0; i < _elements.Length; i++)
            {
                if (i >= tmp.Length)
                {
                    break;
                }
                tmp[i] = _elements[i];
            }
            _elements = tmp;
        }
    }
}
