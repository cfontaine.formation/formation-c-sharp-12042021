﻿using System;

namespace _05_poo
{
    // sealed
    class Voiture
    {
        /// Variables d'instances => Etat

        // On n'a plus besoin de déclarer les variables d'instances Marque,PlaqueIma, CompteurKm, elles seront générées automatiquement par les propriétées automatique
        // private string marque;
        private string _couleur = "Blanc";  // Variable d'instance utilisée par la propriété Couleur (C# 7.0) 
        // private string plaqueIma;
        private int _vitesse;               // Variable d'instance utilisée par la propriété Vitesse (propriété avec une condition)
                                            // private int compteurKm = 50;

        // Variable de classe
        //private static int cptVoiture;    // Remplacer par une propriétée static

        // Propriété automatique => la variable d'instance est générée par le compilateur
        public string Marque { get; protected set; } = "Toyota";
        public string PlaqueIma { get; set; }

        public int CompteurKm { get; set; } = 50;   // On peut donner un valeur par défaut à une propriétée (littéral, expression ou une fonction)

        public static int CptVoiture { get; private set; }  // On peut associer un modificateur d'accès à get et à set. Il doit être plus restritif que le modificateur de la propriété

        // Agrégation
        public Personne Proprietaire { get; set; }

        //Composition
        public Moteur Moteur { get; set; }

        // Propriété
        public int Vitesse
        {
            get
            {
                return _vitesse;
            }
            set
            {
                if (value >= 0)
                {
                    _vitesse = value;
                }
            }
        }

        // Propriété c# 7.0
        public string Couleur
        {
            get => _couleur;
            set => _couleur = value;
        }

        // Accesseur (Java/C++) en C# on utilisera les propriétés
        // Getter (accès en lecture)
        //public int GetVitesse()
        //{
        //    return vitesse;
        //}

        // Setter (accès en écriture)
        //public void SetVitesse(int vitesse)
        //{
        //    if (vitesse >= 0)
        //    {
        //        this.vitesse = vitesse;
        //    }
        //}

        // Constructeurs => On peut surcharger le constructeur
        // Constructeur par défaut
        public Voiture()
        {
            Console.WriteLine("Constructeur par défaut");
            CptVoiture++;
            Moteur = new Moteur(10);
        }

        // this() => Chainnage de constructeur : appel du constructeur par défaut
        public Voiture(string marque, string couleur, int vitesse) : this()
        {
            Console.WriteLine("Constructeur 3 paramètres");
            Marque = marque;
            this._couleur = couleur;
            this._vitesse = vitesse;
        }

        // Chainnage de constructeur : appel du constructeur  Voiture(string marque, string couleur, int vitesse)
        public Voiture(string marque, string couleur, string plaqueIma, int vitesse, int compteurKm) : this(marque, couleur, vitesse)
        {
            Console.WriteLine("Constructeur 5 paramètres");
            PlaqueIma = plaqueIma;
            CompteurKm = compteurKm;
        }

        public Voiture(string couleur, string marque, string plaqueIma, Personne proprietaire) : this()
        {
            _couleur = couleur;
            Marque = marque;
            PlaqueIma = plaqueIma;
            Proprietaire = proprietaire;
        }

        // Exemple de constructeur static: appelé avant la création de la première instance ou le référencement d’un membre statique
        //static Voiture()
        //{
        //    Console.WriteLine("Constructeur static");
        //}


        // Destructeur => libérer des ressources 
        //~Voiture()
        //{
        //    Console.WriteLine("Destructeur voiture");
        //}


        // Méthodes d'instances => comportement
        public void Accelerer(int vAcc)
        {
            if (vAcc > 0)
            {
                _vitesse += vAcc;
            }
        }

        public void Freiner(int vFrn)
        {
            if (vFrn > 0)
            {
                _vitesse -= vFrn;
            }
        }

        public void Arreter()
        {
            _vitesse = 0;
        }

        public bool EstArreter()
        {
            return _vitesse == 0;
        }

        public virtual void Afficher()
        {
            Console.WriteLine($" {Marque} {Couleur} {PlaqueIma} ({Vitesse} KM/h / {CompteurKm} KM)");
        }

        // Méthodes de classes
        public static void TestMethodeClasse()
        {
            Console.WriteLine("Méthode de classe");
            Console.WriteLine(CptVoiture); // On peut accéder dans une méthode de classe à une variable de classe
            //vitesse = 10; // Dans une méthode de classe, on n'a pas accès à une variable d'instance
        }

        public static bool EgaliteVitesse(Voiture va, Voiture vb)
        {
            return va._vitesse == vb._vitesse;  // mais on peut accéder à une variable d'instance par l'intermédiare d'un objet passé en paramètre
        }

    }

}
