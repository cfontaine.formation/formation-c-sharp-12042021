﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    class FakeDaoVoiture
    {
        static List<Voiture> voitures=new List<Voiture>();
        static FakeDaoVoiture()
        {
            voitures.Add(new Voiture("Honda", "Orange", 0));
            voitures.Add(new Voiture("Opel", "Orange", 0));
            voitures.Add(new Voiture("Ford", "Orange", 0));
        }

        public void Delete(Voiture v)
        {
            voitures.Remove(v);
        }

        public void Creer(Voiture v)
        {
            voitures.Add(v);
        }

        public List<Voiture> FindAll()
        {
            return voitures;
        }

    }


}
