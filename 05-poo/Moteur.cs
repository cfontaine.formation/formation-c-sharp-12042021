﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    class Moteur
    {
        public int PuissanceCm { get; set; }

        public Moteur(int puissanceCm)
        {
            PuissanceCm = puissanceCm;
        }
    }
}
