﻿using System;

namespace _05_poo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Voiture
            // Appel d'une méthode de classe
            Voiture.TestMethodeClasse();

            // Appel d'une variable de classe
            Console.WriteLine($"variable de classe= {Voiture.CptVoiture}");

            // Instantiation de la classe Voiture
            Voiture v1 = new Voiture("opel", "noir", 0);
            Console.WriteLine($"variable de classe= {Voiture.CptVoiture}");

            // Accès à une propriété en ecriture (set)
            v1.Vitesse = 10;

            // Accès à une propriété en lecture (get)
            Console.WriteLine(v1.Vitesse);

            // v1.Marque = "Dacia";  // On ne peut pas accéder en écriture à la propriété Marque => pas de set public
            Console.WriteLine(v1.Marque);
            Console.WriteLine(v1.CompteurKm);

            // Appel d’une méthode d’instance
            v1.Accelerer(20);
            Console.WriteLine(v1.Vitesse);
            v1.Freiner(5);
            Console.WriteLine(v1.Vitesse);
            Console.WriteLine(v1.EstArreter());
            v1.Arreter();
            Console.WriteLine(v1.EstArreter());


            Voiture v2 = new Voiture();
            Console.WriteLine($"variable de classe= {Voiture.CptVoiture}");
            v2.Vitesse = 20;
            Console.WriteLine(v2.Vitesse);
            // Test de l'appel du destructeur
            v2 = null;  // En affectant, null à la références v2.Il n'y a plus de référence sur l'objet voiture
                        // Il sera détruit lors de la prochaine execution du garbage collector
                        // Forcer l'appel le garbage collector
            GC.Collect();

            // Initialiseur d'objet
            // Voiture v3 = new Voiture { Marque = "Honda", Couleur = "Rouge", Vitesse = 30, PlaqueIma = "FR-0435-ER" };
            Voiture v3 = new Voiture();
            Console.WriteLine($"variable de classe= {Voiture.CptVoiture}");
            Console.WriteLine(Voiture.EgaliteVitesse(v1, v3));
            v3.Arreter();
            Console.WriteLine(Voiture.EgaliteVitesse(v1, v3));
            #endregion

            #region indexeur
            Tableau tab = new Tableau(5);
            tab[0] = 12;
            tab[1] = 13;
            tab[2] = 1;
            tab[3] = 3;
            tab[4] = 4;
            tab.Add(78);
            Console.WriteLine(tab[5]);
            // Console.WriteLine(tab[12]);

            // Exercice Indexeur
            Point3D p3D = new Point3D(1, 3, 4);
            Console.WriteLine(p3D.X);
            Console.WriteLine(p3D[1]);
            p3D.Y = 12;
            p3D[2] = 12;
            p3D.Afficher();
            #endregion

            #region Classe Imbriquée
            // Si la classe imbriquée est public ou internal, on peut l'instancier en dehors du conteneur (à éviter)
            // Container.Element elm = new Container.Element(); 
            // elm.TestElementClasse();
            // elm.TestElementInstance(c);

            Conteneur conteneur = new Conteneur();
            conteneur.TestImbrique();
            #endregion

            #region  Classe partielle
            // Classe Partielle => classe définit sur plusieurs fichiers
            Form1 f1 = new Form1();
            f1.MethodeA();
            f1.MethodeB();
            #endregion

            #region Classe Statique
            //    Math m = new Math();  // Math est une classe static, on peut pas l'instancier
            // Elle ne contient que des membres de classe

            // Méthode d'extension
            string s = "AZERTY";
            Console.WriteLine(s.Inverser());
            #endregion

            #region Agrégation
            // Agrégation = associer un objet avec un autre
            Adresse adr1 = new Adresse("1, rue Esquermoise", "Lille", "59000");
            Personne per1 = new Personne("John", "Doe", adr1);
            Personne per2 = new Personne("Jane", "Doe", adr1);
            Personne per3 = new Personne("Alan", "Smithee", new Adresse(" 32, Boulevard Vincent Gâche", "Nantes", "44200"));
            Voiture v4 = new Voiture("Vert", "Renault", "fr-5967-SD", per1);
            v4.Proprietaire.Afficher();
            #endregion

            // Héritage
            VoiturePrioritaire vp1 = new VoiturePrioritaire("Renault","Jaune",10,true);
            vp1.Accelerer(10);
            Console.WriteLine(vp1.Vitesse);
            vp1.AllumerGyro();
            Console.WriteLine(vp1.Gyro);

            // Redefinission
            VoiturePrioritaire vp2 = new VoiturePrioritaire("Opel", "Gris", "fr-1234-RT");
            vp2.Afficher();

            Voiture v5 = new VoiturePrioritaire(true);

            #region Exercice Compte Bancaire
            CompteBancaire cb = new CompteBancaire(100.0, per1);
            cb.Afficher();
            cb.Crediter(59.0);
            cb.Afficher();
            cb.Debiter(200.0);
            Console.WriteLine(cb.EstPositif());
            Console.WriteLine(cb.Solde);
            CompteBancaire cb2 = new CompteBancaire(per2);
            cb2.Afficher();


            CompteEpargne ce = new CompteEpargne(per3, 0.75);
            ce.Crediter(1000.0);
            ce.Afficher();
            ce.CalculInterets();
            ce.Afficher();

            CompteEpargne ce2 = new CompteEpargne(0.75);
            ce2.Afficher();

            #endregion

            #region Exercice Point
            Point p1 = new Point();
            Point p2 = new Point(3, 2);
            p1.Deplacer(1, 2);
            p1.Afficher();
            p2.Afficher();
            Console.WriteLine($"Norme p1={p1.Norme()}");
            Console.WriteLine($"Distance p1 et p2={Point.Distance(p1, p2)}");
            #endregion

            #region Cercle
            Cercle c1 = new Cercle(new Point(2, 0), 2);
            Cercle c2 = new Cercle(new Point(9, 0), 3);
            Console.WriteLine($"Collision={Cercle.Collision(c1, c2)}");
            c2.Centre.X = 6;
            Console.WriteLine($"Collision={Cercle.Collision(c1, c2)}");
            Point pt1 = new Point(10, 10);
            Console.WriteLine($"Contenu={c1.Contenu(pt1)}");
            pt1.Deplacer(-9, -9);
            Console.WriteLine($"Contenu={c1.Contenu(pt1)}");
            #endregion
            Console.ReadKey();
        }
    }
}
