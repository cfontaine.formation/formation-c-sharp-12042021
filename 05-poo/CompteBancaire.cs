﻿using System;

namespace _05_poo
{
    // Classe CompteBancaire

    //  Créer une  Classe CompteBancaire

    // Variables d'instances
    // - un solde  par défaut le solde est à 50.0
    // - un iban
    // - un titulaire
    // Méthodes d'instances
    //  Ajouter les méthodes :
    //  - Afficher : qui affiche le compte sous la forme
    //    ____________________________________________
    //    Solde =  
    //    Iban =  
    //    Titulaire=
    //    _____________________________________________
    //
    //  - Crediter: qui ajoute au compte la valeur passé en paramètre
    //  - Debiter: qui retire au compte la valeur passé en paramètre
    //  - estPositif: qui retourne vrai, si le solde est positif
    //
    // Constructeur
    //  Ajouter: 
    //  - un constructeur par défaut
    //  - un constructeur qui initialise le titulaire
    //  - un constructeur qui initialise le titulaire et le solde
    // Variable de classe
    //  - On ajoute une variable de classe qui permet de compter le nombre de compte  créer 
    //  - On utilise cette variable pour initialiser l'iban en concaténant "fr-5962-0000-"  avec elle
    // Propriétés
    // remplacer les variables d'instance par de propriétés pour applique le principe d'encapsulation
    // l'iban et le solde est uniquement accessible en lecture depuis l'extérieur
    // Le compteur de compte va être privée

    class CompteBancaire
    {
        public double Solde { get; protected set; } = 50.0;
        public string Iban { get; }
        public Personne Titulaire { get; set; }

        private static int compteurCompte;

        public CompteBancaire()
        {
            compteurCompte++;
            Iban = "fr-5962-0000-" + compteurCompte;
        }

        public CompteBancaire(Personne titulaire) : this()
        {
            Titulaire = titulaire;
        }

        public CompteBancaire(double solde, Personne titulaire) : this(titulaire)
        {
            Solde = solde;
        }

        public void Crediter(double val)
        {
            if (val > 0.0)
            {
                Solde += val;
            }
        }

        public void Debiter(double val)
        {
            if (val > 0.0)
            {
                Solde -= val;
            }
        }

        public bool EstPositif()
        {
            return Solde >= 0;
        }

        public void Afficher()
        {
            Console.WriteLine("_________________________________");
            Console.WriteLine($"Solde= {Solde}");
            Console.WriteLine($"Iban= {Iban}");
            if (Titulaire != null)
            {
                Console.Write("Titulaire= ");
                Titulaire.Afficher();
            }
            Console.WriteLine("_________________________________");
        }
    }
}
