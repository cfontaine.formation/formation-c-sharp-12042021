﻿namespace _05_poo
{
    class Cercle
    {
        public Point Centre { get; set; }
        public double Rayon { get; set; }

        public Cercle(Point centre, double rayon)
        {
            Centre = centre;
            Rayon = rayon;
        }

        public bool Contenu(Point p)
        {
            return Point.Distance(Centre, p) <= Rayon;
        }

        public static bool Collision(Cercle c1, Cercle c2)
        {
            return Point.Distance(c1.Centre, c2.Centre) < (c1.Rayon + c2.Rayon);
        }

    }
}
