﻿using System;

namespace _09_Surcharge_Operateurs
{
    class Program
    {
        static void Main(string[] args)
        {
            Point p1 = new Point(1, 2);
            Point r = -p1;
            Console.WriteLine(r);
            Point p2 = new Point(-5, 2);
            r = p1 + p2;
            Console.WriteLine(r);
            Console.WriteLine(p1 * 3);
            Point p3 = new Point(2, 2);
            r += p3;    // quand on surcharge un opérateur, l'opérateur composé associé est aussi automatiquement surchargé
            Console.WriteLine(r);
            r *= 4;
            Console.WriteLine(r);

            // Exercice surcharge opérateur
            Fraction f1 = new Fraction(1, 2);
            Console.WriteLine(f1.Calculer());
            Fraction f2 = new Fraction(1, 2);
            Console.WriteLine(f1 + f2);
            Console.WriteLine(f1 * f2);
            Console.WriteLine(f1 * 3);
            Console.WriteLine(f1 == f2);
            Console.WriteLine(f1 == (f2 * 3));
            Console.ReadKey();
        }
    }
}
