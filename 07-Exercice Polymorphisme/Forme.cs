﻿namespace _07_Exercice_Polymorphisme
{
    enum Couleur { VERT, ROUGE, BLEU, ORANGE }
    abstract class Forme
    {
        public Couleur Couleur { get; set; }

        public Forme(Couleur couleur)
        {
            Couleur = couleur;
        }

        public abstract double CalculSurface();
    }
}
