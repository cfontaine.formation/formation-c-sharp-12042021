﻿using System;

namespace _07_Exercice_Polymorphisme
{
    class Program
    {
        static void Main(string[] args)
        {
            Terrain t = new Terrain();
            t.Ajouter(new Cercle(Couleur.BLEU, 1.0));
            t.Ajouter(new Cercle(Couleur.BLEU, 1.0));
            t.Ajouter(new Rectangle(Couleur.VERT, 2.0, 2.0));
            t.Ajouter(new Rectangle(Couleur.VERT, 2.0, 2.0));
            t.Ajouter(new Rectangle(Couleur.VERT, 2.0, 2.0));
            t.Ajouter(new TriangleRectangle(Couleur.ORANGE, 2.0, 2.0));
            t.Ajouter(new Rectangle(Couleur.ROUGE, 1.0, 1.0));
            t.Ajouter(new Rectangle(Couleur.ROUGE, 1.0, 1.0));
            t.Ajouter(new TriangleRectangle(Couleur.ROUGE, 1.0, 1.0));
            Console.WriteLine("Surface");
            Console.WriteLine($" Bleu= {t.SurfaceTotal(Couleur.BLEU)}");
            Console.WriteLine($" Orange= {t.SurfaceTotal(Couleur.ORANGE)}");
            Console.WriteLine($" Vert= {t.SurfaceTotal(Couleur.VERT)}");
            Console.WriteLine($" Rouge= {t.SurfaceTotal(Couleur.ROUGE)}");
            Console.WriteLine($" Total= {t.SurfaceTotal()}");
            Console.ReadKey();
        }
    }
}
