﻿namespace _07_Exercice_Polymorphisme
{
    class Terrain
    {
        private Forme[] formes;

        public int NbForme { get; private set; }

        public Terrain(int size = 10)
        {
            formes = new Forme[size];
        }

        public void Ajouter(Forme f)
        {
            if (NbForme < formes.Length)
            {
                formes[NbForme] = f;
                NbForme++;
            }
        }

        public double SurfaceTotal()
        {
            double surface = 0.0;
            for (int i = 0; i < NbForme; i++)
            {
                surface += formes[i].CalculSurface();
            }
            return surface;
        }

        public double SurfaceTotal(Couleur couleur)
        {
            double surface = 0.0;
            for (int i = 0; i < NbForme; i++)
            {
                if (formes[i].Couleur == couleur)
                {
                    surface += formes[i].CalculSurface();
                }
            }
            return surface;
        }
    }
}
