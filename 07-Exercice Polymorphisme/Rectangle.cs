﻿namespace _07_Exercice_Polymorphisme
{
    class Rectangle : Forme
    {
        public double Largeur { get; set; }
        public double Longueur { get; set; }

        public Rectangle(Couleur couleur, double largeur, double longueur) : base(couleur)
        {
            Largeur = largeur;
            Longueur = longueur;
        }
        // sealed 
        public override double CalculSurface()
        {
            return Largeur * Longueur;
        }
    }
}
