﻿namespace _07_Exercice_Polymorphisme
{
    class TriangleRectangle : Rectangle
    {
        public TriangleRectangle(Couleur couleur, double largeur, double longueur) : base(couleur, largeur, longueur)
        {
        }

        public override double CalculSurface()
        {
            return base.CalculSurface() / 2.0;
        }
    }
}
