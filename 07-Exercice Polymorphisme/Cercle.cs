﻿using System;

namespace _07_Exercice_Polymorphisme
{
    class Cercle : Forme
    {
        public double Rayon { get; set; }

        public Cercle(Couleur couleur, double rayon) : base(couleur)
        {
            Rayon = rayon;
        }

        public override double CalculSurface()
        {
            return Math.PI * Rayon*Rayon;
        }
    }
}
