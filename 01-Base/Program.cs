﻿using System;
using System.Text;

namespace _01_Base
{

    // Une enumération est un ensemble de constante
    // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
    enum Direction : short { NORD = 90, EST = 0, SUD = 270, OUEST = 180 };

    [Flags]
    enum JOURS
    {
        LUNDI = 1,
        MARDI = 2,
        MERCREDI = 4,
        JEUDI = 8,
        VENDREDI = 16,
        SAMEDI = 32,
        DIMANCHE = 64,
        WEEKEND = SAMEDI | DIMANCHE
    };

    // Définition d'une structure
    struct Point
    {
        public int X;
        public int Y;
    }
    class Program
    {
        static void Main(string[] args)
        {
            #region Variable et type simple
            // Déclaration d'une variable   type nomVariable;
            int i;
            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable locale non initialisée

            // Initialisation    nomVariable = valeur
            i = 42;
            Console.WriteLine(i);

            // Déclaration multiple de variable
            double hauteur, largeur;
            hauteur = 2.0;
            largeur = 123.0;
            Console.WriteLine(hauteur + " " + largeur); // + => concaténation

            // Déclaration et initialisation de variable    type nomVariable = valeur;
            int j = 12;
            int j1 = 1, j2 = 4;
            Console.WriteLine(j + " " + j1 + " " + j2);

            // Littéral booléen
            bool tst = false; // true
            Console.WriteLine(tst);

            // Littéral caractère
            char chr = 's';
            char chrutf8 = '\u0045';    // Caractère en UTF-8
            char chrHexUtf8 = '\x45';
            Console.WriteLine(chr + " " + chrutf8 + " " + chrHexUtf8);

            // Littéral entier -> int par défaut
            long l = 12345678L; // L->Littéral long
            uint ui = 1234U;    // U -> Litéral unsigned
            Console.WriteLine(l + " " + ui);

            // Littéral entier -> chagement de base
            int dec = 10;       // décimal (base 10) par défaut
            int hexa = 0xff12;  // 0x => héxadécimal (base 16)
            int bin = 0b101011; // 0b => binaire 
            Console.WriteLine(dec + " " + hexa + " " + bin);

            // Littéral nombre à virgule flottante -> double par défaut
            float f = 123.5F;       // F -> Littéral float
            decimal d2 = 123.5M;    // M -> Littéral decimal
            Console.WriteLine(f + " " + d2);

            // Littéral nombre à virgule flottante
            double d3 = 100.4;
            double dExp = 3.5e3;
            Console.WriteLine(d3 + " " + dExp);

            // Séparateur _
            double sep = 1_000_000.5;
            // double sd = _100_._00_;  // pas de séparateur en début, en fin , avant et après la virgule
            Console.WriteLine(sep);

            // Type implicite -> var
            var v1 = 10.2;      // v1 -> double
            var v2 = "azerty";  // v2 -> string
            Console.WriteLine(v1 + " " + v2);

            //  avec @ on peut utiliser les mots réservés pour les nom de variable (à éviter) 
            int @var = 12;
            Console.WriteLine(@var);
            #endregion

            #region Conversion
            // Transtypage implicite: ( pas de perte de donnée)
            // type inférieur => type supérieur
            int tii = 123;
            long til = tii;
            double tid = tii;

            // Transtypage explicite: cast = (nouveauType)
            double ted = 12.3;
            int tei = (int)ted;
            Console.WriteLine(ted + " " + tei);

            // Dépassement de capacité
            short sh1 = 300;            // 00000001 00101100    300
            sbyte sb1 = (sbyte)sh1;     //          00101100    44
            Console.WriteLine(sh1 + " " + sb1);

            // checked / unchecked
            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral

            //checked
            //{
            //    sb1 = (sbyte)sh1;
            //    Console.WriteLine(sh1 + " " + sb1);    // avec checked une exception est générée, s'il y a un dépassement de capacité
            //}

            unchecked // (par défaut )
            {
                sb1 = (sbyte)sh1;
                Console.WriteLine(sh1 + " " + sb1);     // plus de vérification de dépassement de capacité
            }

            // Fonction de convertion
            // La classe Convert contient des méthodes statiques permettant de convertir un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérique
            int fc1 = 42;
            double cnv1 = Convert.ToDouble(fc1);    // Convertion d'un entier en double

            string str = "123";
            int cnv2 = Convert.ToInt32(str);
            Console.WriteLine(cnv1 + " " + cnv2);   // Convertion d'une chaine de caractère  en entier

            // Conversion d'une chaine de caractères en entier
            Console.WriteLine(Int32.Parse("234"));
            // Console.WriteLine(Int32.Parse("azerty"));    //  Erreur => génère une exception
            int cnv3;
            bool tstCnv = Int32.TryParse("123", out cnv3);  // Retourne true et la convertion est affecté à cnv3
            Console.WriteLine(tstCnv + " " + cnv3);

            tstCnv = Int32.TryParse("azerty", out cnv3);    // Erreur => retourne false , 0 est affecté à cnv3
            Console.WriteLine(tstCnv + " " + cnv3);
            #endregion

            #region Type référence
            StringBuilder s1 = new StringBuilder("Hello");
            StringBuilder s2 = null;
            Console.WriteLine(s1 + " " + s2);
            s2 = s1;
            Console.WriteLine(s1 + " " + s2);
            s2 = null;
            s1 = null;  //str1 et str2 sont égales à null
                        // Il n'y a plus de référence sur l'objet, il éligible à la destruction par le garbage collector

            //DateTime d;
            //s1 = d;

            #endregion

            #region Format de chaine de caractères
            int xi = 1;
            int yi = 2;
            string resFormat = string.Format("xi={0} yi={1}", xi, yi); ; // on peut définir directement le format dans la mèthode WriteLine 
            Console.WriteLine(resFormat);

            Console.WriteLine("xi={0} yi={1}", xi, yi);

            Console.WriteLine($"xi={xi} yi={yi}");

            // Caractères spéciaux
            // \n       Nouvelle ligne
            // \r       Retour chariot
            // \f       Saut de page
            // \t       Tabulation horizontale
            // \v       Tabulation verticale
            // \0       Caractère nul
            // \a 	    Alerte
            // \b       Retour arrière
            // \\       Backslash
            // \'       Apostrophe
            // \"       Guillemet

            // Chaînes textuelles => @ devant une littérale chaine de caractères
            // Conserve les retours à la ligne, n'interpréte pas les caractères spéciaux (utile pour les chemins de fichiers) 
            Console.WriteLine(@"c:\tmp\newfile.txt");

            // Conversion d'un nombre sous forme de chaine dans une autre base
            Console.WriteLine(Convert.ToString(42, 2));     // affichage en binaire
            Console.WriteLine(Convert.ToString(42, 16));    // affichage en héxadécimal
            #endregion

            // Saisir une valeur au clavier
            // string strl = Console.ReadLine();
            // Console.WriteLine(strl);

            #region Exercices
            // Exercice: Salutation
            // Faire un programme qui:
            // - Affiche le message: Entrer votre nom
            // - Permet de saisir le nom
            // - Affiche Bonjour, complété du nom saisie
            Console.WriteLine("Entrer votre nom");
            string nom = Console.ReadLine();
            Console.WriteLine($"Bonjour, {nom}");

            // Exercice: Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 + 3 = 4
            string val = Console.ReadLine();
            int va = Convert.ToInt32(val);
            int vb = Convert.ToInt32(Console.ReadLine());
            int res = va + vb;
            Console.WriteLine($"{va} + {vb} = {res}");
            #endregion

            #region Nullable
            // Nullable : permet d'utiliser une valeur null avec un type valeur
            double? n = null;
            Console.WriteLine(n.HasValue);   // La propriété HasValue retourne true si n contient une valeur (!= null) 
            n = 12.3;       //Conversion implicite 
            Console.WriteLine(n.HasValue);
            double d4 = (double)n;      // Pour récupérer la valeur on peur faireun cast
            double d5 = n.Value;        // ou on peut utiliser la propriété Value
            Console.WriteLine($"{d4} {d5}");
            #endregion

            // Constante
            const double PI = 3.14;
            //PI = 5;   // Erreur: on ne peut pas modifier la valeur d'une constante
            Console.WriteLine(PI * 2);

            #region Opérateur
            // Opérateur arithméthique
            int op1 = 1;
            int op2 = 4;
            int res2 = op1 + op2;
            Console.WriteLine(res2);
            res2 = res % 2;     // % => modulo (reste de division entière)
            Console.WriteLine(res2);

            // Opérateur d'incrémentation
            // Pré-incrémentation
            int inc = 0;
            res = ++inc; //inc=1 res=1
            Console.WriteLine($"inc={inc} res={res}");

            // Post-incrémentation
            inc = 0;
            res = inc++; //inc=1 res=0
            Console.WriteLine($"inc={inc} res={res}");

            // Affectation composée
            double ac = 4.5;
            ac += 10.0; // ac=ac+10.0;
            Console.WriteLine(ac);

            // Opérateur de comparaison
            bool testC = ac > 10.0; // Une comparaison a pour résultat un booléen
            Console.WriteLine(testC);
            testC = inc == 1;
            Console.WriteLine(testC);

            // Opérateur logique
            testC = !(inc == 1);
            Console.WriteLine(testC);

            // Opérateur court-circuit && et ||
            // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            testC = ac < 10.0 && inc == 1;  // comme ac < 10.0 est faux,  inc == 1 n'est pas évalué
            Console.WriteLine(testC);
            // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            testC = inc == 1 || ac < 10.0;
            Console.WriteLine(testC);

            // Opérateur Binaires (bit à bit)
            byte b = 0b11010;
            Console.WriteLine(Convert.ToString(~b, 2));         // Opérateur ~ => complémént: 1-> 0 et 0-> 1
            Console.WriteLine(Convert.ToString(b & 0b110, 2));  //          et => 010   2
            Console.WriteLine(Convert.ToString(b | 0b110, 2));  //          ou => 11110 30
            Console.WriteLine(Convert.ToString(b ^ 0b110, 2));  // ou exclusif => 11100 28
            Console.WriteLine(Convert.ToString(b >> 1));         // Décalage à droite de 1   1101
            Console.WriteLine(Convert.ToString(b << 2));        // Décalage à gauche de 2   1101000

            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null ;
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string str3 = "hello";
            string str31 = str3 ?? "Default";
            Console.WriteLine(str31);   // Default
            str3 = null;
            str31 = str3 ?? "Default";
            Console.WriteLine(str31);   // hello
            #endregion

            #region Promotion numérique
            double prnd = 123.45;
            int prni = 11;
            double prnres = prnd + prni;  // prni est promu en double => Le type le + petit est promu vers le + grand type des deux
            Console.WriteLine(prnres);
            Console.WriteLine(prni / 2); // 5
            Console.WriteLine(prni / 2.0); // 5.5 prni est promu en double

            sbyte b1 = 1;
            sbyte b2 = 2;       // sbyte, byte, short, ushort, char sont promus en int
            int b3 = b1 + b2;   // b1 et b2 sont promus en int
            #endregion


            #region Enumération
            // dir est une variable qui ne pourra accepter que les valeurs de l'enum Direction
            Direction dir = Direction.SUD;

            // enum ->  entier (cast)
            short dirInt = (short)dir;
            Console.WriteLine(dirInt);

            // La méthode toString convertit une constante enummérée en une chaine de caractère
            Console.WriteLine(dir.ToString());

            // entier -> enum (cast)
            short s = 180;
            if (Enum.IsDefined(typeof(Direction), s))   // Permet de tester si la valeur entière existe dans l'enumération
            {
                dir = (Direction)s;
                Console.WriteLine(dir.ToString());  //OUEST
            }

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            dir = (Direction)Enum.Parse(typeof(Direction), "SUD");
            Console.WriteLine(dir.ToString());
            // dir = (Direction)Enum.Parse(typeof(Direction), "SUD2"); // si la chaine n'existe pas dans l'énumation => exception
            //Console.WriteLine(dir.ToString());

            // Énumération comme indicateurs binaires
            JOURS jours = JOURS.LUNDI | JOURS.MARDI;
            if ((jours & JOURS.LUNDI) != 0)      // teste la présence de LUNDI
            {
                Console.WriteLine("Lundi");
            }
            if ((jours & JOURS.MERCREDI) != 0)   // teste la présence de MERCREDI
            {
                Console.WriteLine("Mercredi");
            }
            jours |= JOURS.SAMEDI;
            if ((jours & JOURS.WEEKEND) != 0)
            {
                Console.WriteLine("Week-end");
            }
            #endregion

            #region Structure
            Point p1;
            p1.X = 1;   // accès au champs X de la structure
            p1.Y = 2;

            Console.WriteLine($"P1( x={p1.X} y= {p1.Y})");

            Point p2 = p1;
            Console.WriteLine($"P2( x={p2.X} y= {p2.Y})");

            if (p1.X == p2.X && p1.Y == p2.Y)
            {
                Console.WriteLine("Les Points sont égaux");
            }

            p2.X = 3;
            Console.WriteLine($"P1 (x={p1.X} y= {p1.Y})");
            Console.WriteLine($"P2 (x={p2.X} y= {p2.Y})");
            #endregion

            Console.ReadKey();
        }
    }
}
